package com.test.project.elasticsearch;

import com.test.project.models.Order;
import com.test.project.models.OrderItem;
import com.test.project.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Denis Dulyak on 06.09.2016.
 */
@Service
public class ElasticOrderService {

    @Autowired
    private ElasticOrderRepository repository;

    public ElasticOrder addOrderToElasticsearch(Order order) {
        ElasticOrder elasticOrder = new ElasticOrder();
        elasticOrder.setId(order.getId());
        elasticOrder.setTotalCharge(order.getTotalCharge());
        elasticOrder.setDate(order.getDate());

        List<ElasticOrderItem> orderItems = new ArrayList<>();
        for (OrderItem orderItem : order.getOrderItems()) {
            ElasticOrderItem item = new ElasticOrderItem();
            item.setQuantity(orderItem.getQuantity());

            Product product = orderItem.getProduct();
            item.setProduct(new ElasticProduct(
                    product.getName(),
                    product.getPrice(),
                    product.getSku(),
                    product.getCategory().getName())
            );

            orderItems.add(item);
        }

        elasticOrder.setOrderItems(orderItems);
        return repository.save(elasticOrder);
    }

    public List<ElasticOrder> findByProductName(String name) {
        return repository.findByOrderItemsProductNameContaining(name);
    }
}
