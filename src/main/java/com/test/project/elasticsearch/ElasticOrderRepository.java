package com.test.project.elasticsearch;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

/**
 * Created by Denis Dulyak on 07.09.2016.
 */
public interface ElasticOrderRepository extends ElasticsearchRepository<ElasticOrder, Long> {

    List<ElasticOrder> findByOrderItemsProductNameContaining(String name);
}
