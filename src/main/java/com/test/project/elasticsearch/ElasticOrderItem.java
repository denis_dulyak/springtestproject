package com.test.project.elasticsearch;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Denis Dulyak on 06.09.2016.
 */
@Getter
@Setter
public class ElasticOrderItem {

    private Integer quantity;
    private ElasticProduct product;
}
