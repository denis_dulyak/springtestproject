package com.test.project.elasticsearch;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by Denis Dulyak on 06.09.2016.
 */
@Getter
@Setter
@Document(indexName = "elastic_test", shards = 1, replicas = 0, refreshInterval = "-1")
public class ElasticOrder {

    @Id
    private Long id;
    private BigDecimal totalCharge;
    private Date date;
    private List<ElasticOrderItem> orderItems;
}
