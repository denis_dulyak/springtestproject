package com.test.project.elasticsearch;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * Created by Denis Dulyak on 07.09.2016.
 */
@Getter
@Setter
public class ElasticProduct {

    private String name;
    private BigDecimal price;
    private Long sku;
    private String category;

    public ElasticProduct() {
    }

    public ElasticProduct(String name, BigDecimal price, Long sku, String category) {
        this.name = name;
        this.price = price;
        this.sku = sku;
        this.category = category;
    }
}
