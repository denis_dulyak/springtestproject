package com.test.project.utils;

import com.test.project.models.Order;
import com.test.project.models.OrderItem;

import java.math.BigDecimal;

/**
 * Created by Denis Dulyak on 09.09.2016.
 */
public class PriceUtil {

    public static void calculateOrderTotalCharge(Order order) {
        BigDecimal total = new BigDecimal("0");
        for (OrderItem item : order.getOrderItems()) {
            total = total.add(
                item.getProduct().getPrice().multiply(new BigDecimal(item.getQuantity()))
            );
        }

        order.setTotalCharge(total);
    }
}
