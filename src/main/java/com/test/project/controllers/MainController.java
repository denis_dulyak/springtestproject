package com.test.project.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Denis Dulyak on 06.09.2016.
 */
@RestController
public class MainController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String main() {
        return "Hi, it's a Spring REST application. <br/><a href='/swagger-ui.html'>Swagger</a>";
    }
}
