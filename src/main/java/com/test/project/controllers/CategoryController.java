package com.test.project.controllers;

import com.test.project.models.Category;
import com.test.project.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Denis Dulyak on 09.09.2016.
 */
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryRepository repository;

    @RequestMapping(value = "/find-by-id", method = RequestMethod.GET)
    public Category findById(@RequestParam Long id) {
        return repository.findOne(id);
    }

    @RequestMapping(value = "/find-all", method = RequestMethod.GET)
    public List<Category> findAll() {
        return repository.findAll();
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Category save(@RequestBody Category category) {
        return repository.save(category);
    }

    @RequestMapping(value = "/delete-by-id", method = RequestMethod.DELETE)
    public String delete(@RequestParam Long id) {
        repository.delete(id);
        return repository.exists(id) ? "Failed" : "Success";
    }
}
