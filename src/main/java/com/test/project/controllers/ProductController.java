package com.test.project.controllers;

import com.test.project.models.Product;
import com.test.project.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Denis Dulyak on 09.09.2016.
 */
@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductRepository repository;

    @RequestMapping(value = "/find-by-id", method = RequestMethod.GET)
    public Product findById(@RequestParam Long id) {
        return repository.findOne(id);
    }

    @RequestMapping(value = "/find-all", method = RequestMethod.GET)
    public List<Product> findAll() {
        return repository.findAll();
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Product save(@RequestBody Product product) {
        return repository.save(product);
    }

    @RequestMapping(value = "/delete-by-id", method = RequestMethod.DELETE)
    public String delete(@RequestParam Long id) {
        repository.delete(id);
        return repository.exists(id) ? "Failed" : "Success";
    }
}
