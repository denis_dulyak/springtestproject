package com.test.project.controllers;

import com.test.project.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Denis Dulyak on 06.09.2016.
 */
@RestController
@RequestMapping("/report")
public class ReportController {

    @Autowired
    private OrderRepository orderRepository;

    @RequestMapping(value = "/by-day", method = RequestMethod.GET)
    public Map<String, BigDecimal> groupByDay() {
        return orderRepository.getDailyReport().stream().collect(Collectors.toMap(
                objs -> objs[0] + "",
                objs -> new BigDecimal(objs[1] + ""))
        );
    }
}
