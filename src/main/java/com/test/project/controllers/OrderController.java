package com.test.project.controllers;

import com.test.project.elasticsearch.ElasticOrder;
import com.test.project.elasticsearch.ElasticOrderService;
import com.test.project.models.Order;
import com.test.project.models.OrderItem;
import com.test.project.repositories.OrderRepository;
import com.test.project.repositories.ProductRepository;
import com.test.project.utils.PriceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Denis Dulyak on 07.09.2016.
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderRepository repository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ElasticOrderService elasticOrderService;

    @RequestMapping(value = "/find-by-id", method = RequestMethod.GET)
    public Order findById(@RequestParam Long id) {
        return repository.findOne(id);
    }

    @RequestMapping(value = "/find-all", method = RequestMethod.GET)
    public List<Order> findAll() {
        return repository.findAll();
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Order save(@RequestBody @Valid Order order) {
        for (OrderItem item : order.getOrderItems()) {
            item.setOrder(order);
            item.setProduct(productRepository.findOne(item.getProduct().getId()));
        }

        PriceUtil.calculateOrderTotalCharge(order);

        order = repository.save(order);
        elasticOrderService.addOrderToElasticsearch(order);

        return order;
    }

    @RequestMapping(value = "/delete-by-id", method = RequestMethod.DELETE)
    public String delete(@RequestParam Long id) {
        repository.delete(id);
        return repository.exists(id) ? "Failed" : "Success";
    }

    @RequestMapping(value = "/find-by-product-name", method = RequestMethod.GET)
    public List<ElasticOrder> findByProductName(@RequestParam String name) {
        return elasticOrderService.findByProductName(name);
    }
}
