package com.test.project.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created by Denis Dulyak on 06.09.2016.
 */
@Getter
@Setter
@Entity
@Table
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "The name is required.")
    @Column(nullable = false)
    private String name;

    @NotNull(message = "The price is required.")
    @Column(nullable = false)
    private BigDecimal price;

    @NotNull(message = "The sku is required.")
    @Column(nullable = false, unique = true)
    private Long sku;

    @ManyToOne
    @JoinColumn
    private Category category;
}
