package com.test.project.repositories;

import com.test.project.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Denis Dulyak on 06.09.2016.
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
}
