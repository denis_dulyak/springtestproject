package com.test.project.repositories;

import com.test.project.models.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Denis Dulyak on 06.09.2016.
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    @Query(value = "SELECT CAST(o.date as date) AS date, SUM(o.total_charge) AS total_charge FROM orders AS o GROUP BY date", nativeQuery = true)
    List<Object[]> getDailyReport();
}
